## Android task

Implement a screen showing a list of posts, where each item displays post title and body. Posts
should be cached locally with cache validity time of 5 minutes. Also there should be an option
to refresh the list manually.

For fetching posts use this api: https://jsonplaceholder.typicode.com/posts.

Click on a post should show a popup with details (name and email) of the owner (fetched from
api: https://jsonplaceholder.typicode.com/users/{userId}) and an option to delete the post
locally.

---

## Application architecture

Application is based on MVVM architecture and repository pattern

Application is using:

1. Dependency injection - dagger 2
2. Data and view binding
3. Architecture components: LiveData, ViewModel, Room Persistence
4. Retrofit2 & Gson for constructing the REST API