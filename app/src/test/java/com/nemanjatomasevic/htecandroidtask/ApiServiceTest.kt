package com.nemanjatomasevic.htecandroidtask

import android.content.Context
import com.nemanjatomasevic.htecandroidtask.model.UserModel
import com.nemanjatomasevic.htecandroidtask.networking.UsersApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ApiServiceTest {

    private var mockWebServer = MockWebServer()
    private lateinit var userApiService: UsersApi

    @Before
    fun setup() {
        mockWebServer.start()

        userApiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(UsersApi::class.java)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getUser() {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(userResponse)

        mockWebServer.enqueue(response)

        var user: Response<UserModel>? = null
        runBlocking {
            user = userApiService.getUserById(1)
        }

        assertEquals(user?.body()?.address?.geo?.lat, "-37.3159")
        assertEquals(user?.body()?.company?.name, "Romaguera-Crona")
    }
}