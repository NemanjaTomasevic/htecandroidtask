package com.nemanjatomasevic.htecandroidtask.di

import android.content.Context
import com.nemanjatomasevic.htecandroidtask.view.PostFeedFragment
import dagger.BindsInstance
import dagger.Component

@Component
interface AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(fragment: PostFeedFragment)
}