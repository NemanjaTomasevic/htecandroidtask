package com.nemanjatomasevic.htecandroidtask.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nemanjatomasevic.htecandroidtask.model.PostModel
import com.nemanjatomasevic.htecandroidtask.model.PostsDAO

@Database(entities = [PostModel::class], version = 1, exportSchema = false)
abstract class PostsRoomDatabase: RoomDatabase() {

    abstract fun postsDao(): PostsDAO

    companion object {
        private var INSTANCE: PostsRoomDatabase? = null

        fun getDatabase(context: Context): PostsRoomDatabase {
            val tmpInstance = INSTANCE
            if(tmpInstance != null) {
                return tmpInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PostsRoomDatabase::class.java,
                    "posts_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}