package com.nemanjatomasevic.htecandroidtask

import android.app.Application
import com.nemanjatomasevic.htecandroidtask.di.AppComponent
import com.nemanjatomasevic.htecandroidtask.di.DaggerAppComponent

class PostFeedApplication: Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext)
    }
}