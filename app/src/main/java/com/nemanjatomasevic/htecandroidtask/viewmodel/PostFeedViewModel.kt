package com.nemanjatomasevic.htecandroidtask.viewmodel

import android.content.Context
import androidx.lifecycle.*
import com.nemanjatomasevic.htecandroidtask.model.PostModel
import com.nemanjatomasevic.htecandroidtask.model.UserModel
import com.nemanjatomasevic.htecandroidtask.repository.Repository
import com.nemanjatomasevic.htecandroidtask.utils.DataValidationUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class PostFeedViewModel @Inject constructor(
    private val context: Context,
    private val repository: Repository
) : ViewModel() {

    private var _postFeed = MutableLiveData<List<PostModel>>()
    private var postFeed: LiveData<List<PostModel>> = _postFeed
    private val isDataValid = DataValidationUtils.isDataValid(context)

    fun getFeed(): LiveData<List<PostModel>> {
        postFeed = repository.posts

        if (!isDataValid) {
            viewModelScope.launch(Dispatchers.IO) {
                repository.fetchPosts(context)
            }
        }

        return postFeed
    }

    fun fetchFeed(): LiveData<List<PostModel>> = liveData {
        val data = repository.fetchPosts(context)
        emit(data.value ?: ArrayList<PostModel>())
    }


    fun getUserById(userId: Int): LiveData<UserModel>? = liveData {
        val user = repository.fetchUserById(userId)
        user?.value?.let { emit(it) }
    }


    fun deletePost(postId: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.deletePost(postId)
    }
}