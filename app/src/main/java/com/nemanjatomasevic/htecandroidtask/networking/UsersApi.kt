package com.nemanjatomasevic.htecandroidtask.networking

import com.nemanjatomasevic.htecandroidtask.model.UserModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface UsersApi {

    @GET("/users/{id}")
    suspend fun getUserById(@Path("id") id: Int): Response<UserModel>
}