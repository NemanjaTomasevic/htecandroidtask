package com.nemanjatomasevic.htecandroidtask.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClientInstance {

    companion object {
        private const val BASE_URL: String = "https://jsonplaceholder.typicode.com"

        private var instance: Retrofit? = null

        fun getInstance(): Retrofit? {
            if (instance == null) {
                instance = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }

            return instance
        }
    }
}