package com.nemanjatomasevic.htecandroidtask.networking

import com.nemanjatomasevic.htecandroidtask.model.PostModel
import retrofit2.Response
import retrofit2.http.GET

interface PostsApi {

    @GET("/posts")
    suspend fun getAllPosts(): Response<ArrayList<PostModel>>
}