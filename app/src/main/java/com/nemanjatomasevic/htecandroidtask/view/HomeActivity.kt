package com.nemanjatomasevic.htecandroidtask.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nemanjatomasevic.htecandroidtask.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
    }
}