package com.nemanjatomasevic.htecandroidtask.view

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.nemanjatomasevic.htecandroidtask.PostFeedApplication
import com.nemanjatomasevic.htecandroidtask.R
import com.nemanjatomasevic.htecandroidtask.databinding.PostFeedFragmentBinding
import com.nemanjatomasevic.htecandroidtask.model.UserModel
import com.nemanjatomasevic.htecandroidtask.viewmodel.PostFeedViewModel
import javax.inject.Inject

class PostFeedFragment : Fragment() {

    private var _binding: PostFeedFragmentBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var feedViewModel: PostFeedViewModel

    private val postsAdapter: PostsAdapter by lazy {
        PostsAdapter()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (requireActivity().application as PostFeedApplication).appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = PostFeedFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        setupSwipeRefreshLayout()
        setupPostFeedRecyclerView(binding.postFeedRecyclerView)

        return view
    }

    override fun onStart() {
        super.onStart()

        getFeed()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupSwipeRefreshLayout() {
        binding.postFeedSwipeRefreshLayout.setOnRefreshListener {
            feedViewModel.fetchFeed().observe(viewLifecycleOwner, Observer {
                it?.let { binding.postFeedSwipeRefreshLayout.isRefreshing = false }
            })
        }
    }

    private fun setupPostFeedRecyclerView(postFeedRecycler: RecyclerView) {
        postFeedRecycler.layoutManager = LinearLayoutManager(context)
        postFeedRecycler.adapter = postsAdapter
        postFeedRecycler.itemAnimator = DefaultItemAnimator()

        postsAdapter.onPostClick = {
            getUser(it.id, it.userId)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.post_feed_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.post_feed_menu_refresh -> {
                feedViewModel.fetchFeed().observe(viewLifecycleOwner, Observer {  })
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun getFeed() {
        feedViewModel.getFeed().observe(viewLifecycleOwner,
            Observer {
                it?.let { postsAdapter.updatePostFeed(ArrayList(it)) }
            })
    }


    private fun getUser(postId: Int, userId: Int) {
        feedViewModel.getUserById(userId)?.observe(viewLifecycleOwner,
            Observer {
                it?.let {
                    displayUserDetailsDialog(postId, it)
                }
            })
    }

    private fun deletePost(userId: Int) {
        feedViewModel.deletePost(userId)
    }

    private fun displayUserDetailsDialog(postId: Int, userDetails: UserModel) {
        context?.let {
            val message = String.format(it.resources.getString(R.string.postFeed_userDetails_text),
                userDetails.name,
                userDetails.email)

            MaterialAlertDialogBuilder(it)
                .setTitle(it.resources.getString(R.string.userDetails_alertDialog_title))
                .setMessage(message)
                .setPositiveButton(it.resources.getString(R.string.userDetails_alertDialog_positiveButtonText)) { dialog, _ ->
                    dialog.dismiss()
                }
                .setNeutralButton(it.resources.getString(R.string.userDetails_alertDialog_deletePostButtonText)) { _, _ ->
                    deletePost(postId)
                }.show()
        }
    }
}