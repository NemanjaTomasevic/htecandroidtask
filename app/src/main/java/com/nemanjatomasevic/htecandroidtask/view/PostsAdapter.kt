package com.nemanjatomasevic.htecandroidtask.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.nemanjatomasevic.htecandroidtask.databinding.PostItemBinding
import com.nemanjatomasevic.htecandroidtask.model.PostModel

class PostsAdapter : RecyclerView.Adapter<PostsViewHolder>() {

    var onPostClick: ((PostModel) -> Unit)? = null
    private var postFeed = ArrayList<PostModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val postItemBinding = PostItemBinding.inflate(layoutInflater, parent, false)
        return PostsViewHolder(postItemBinding)
    }

    override fun getItemCount(): Int {
        return postFeed.size
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val post = postFeed[position]
        holder.bind(post)
        holder.itemView.setOnClickListener { onPostClick?.invoke(post) }
    }

    fun updatePostFeed(posts: ArrayList<PostModel>) {
        val diffCallback = PostsDiffCallback(this.postFeed, posts)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.postFeed.clear()
        this.postFeed.addAll(posts)
        diffResult.dispatchUpdatesTo(this)
    }
}

class PostsViewHolder(private val binding: PostItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(post: PostModel) {
        binding.post = post
        binding.executePendingBindings()
    }
}

class PostsDiffCallback(private val oldList: ArrayList<PostModel>, private val newList: ArrayList<PostModel>): DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val(idOld, userIdOld, titleOld, bodyOld) = oldList[oldItemPosition]
        val(idNew, userIdNew, titleNew, bodyNew) = newList[newItemPosition]

        return idOld == idNew
                &&  userIdOld == userIdNew
                && titleOld == titleNew
                && bodyOld == bodyNew
    }
}