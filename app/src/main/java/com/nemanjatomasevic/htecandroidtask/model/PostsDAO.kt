package com.nemanjatomasevic.htecandroidtask.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PostsDAO {

    @Query("SELECT * FROM posts_table")
    fun getAllPosts(): LiveData<List<PostModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updatePosts(posts: List<PostModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPost(post: PostModel)

    @Query("DELETE FROM posts_table WHERE id=:postId")
    suspend fun deletePost(postId: Int)
}