package com.nemanjatomasevic.htecandroidtask.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "posts_table")
data class PostModel(@SerializedName("id") @PrimaryKey val id: Int,
                     @SerializedName("userId") val userId: Int,
                     @SerializedName("title") val title: String? = "",
                     @SerializedName("body") val body: String? = "")