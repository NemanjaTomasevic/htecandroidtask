package com.nemanjatomasevic.htecandroidtask.utils

import android.content.Context
import com.nemanjatomasevic.htecandroidtask.R
import java.sql.Timestamp

class DataValidationUtils {

    companion object {
        private const val prefsName = "preferences_name"
        private const val dataRefreshTimestamp = "data_refresh_timestamp"

        fun isDataValid(context: Context): Boolean {
            var isValid = false

            val sharedPreferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
            val refreshTimestamp = sharedPreferences.getLong(dataRefreshTimestamp, -1)

            if(refreshTimestamp > 0) {
                val currentTimeTimestamp: Long = Timestamp(System.currentTimeMillis()).time

                val timestampDifference = currentTimeTimestamp - refreshTimestamp
                val lastCacheUpdateInMinutes = (timestampDifference / 1000) / 60

                val cacheValidityTimeInMinutes = context.resources.getInteger(R.integer.CACHE_VALIDITY_TIME_IN_MINUTES)

                isValid = lastCacheUpdateInMinutes < cacheValidityTimeInMinutes
            }

            return isValid
        }

        fun setDataRefreshTime(context: Context) {
            val sharedPreferences = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()

            val currentTimeTimestamp: Long = Timestamp(System.currentTimeMillis()).time
            editor.putLong(dataRefreshTimestamp, currentTimeTimestamp)
            editor.apply()
        }
    }
}