package com.nemanjatomasevic.htecandroidtask.repository

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.nemanjatomasevic.htecandroidtask.database.PostsRoomDatabase
import com.nemanjatomasevic.htecandroidtask.utils.DataValidationUtils
import com.nemanjatomasevic.htecandroidtask.model.PostModel
import com.nemanjatomasevic.htecandroidtask.model.PostsDAO
import com.nemanjatomasevic.htecandroidtask.model.UserModel
import com.nemanjatomasevic.htecandroidtask.networking.PostsApi
import com.nemanjatomasevic.htecandroidtask.networking.RetrofitClientInstance
import com.nemanjatomasevic.htecandroidtask.networking.UsersApi
import kotlinx.coroutines.*
import retrofit2.HttpException
import javax.inject.Inject

class Repository @Inject constructor(private val context: Context) {

    private val postsDao: PostsDAO = PostsRoomDatabase.getDatabase(context).postsDao()
    val posts: LiveData<List<PostModel>>

    init {
        posts = postsDao.getAllPosts()
    }

    suspend fun fetchPosts(context: Context): LiveData<List<PostModel>> {
        val responseResult = MutableLiveData<List<PostModel>>()

        val service = RetrofitClientInstance.getInstance()?.create(PostsApi::class.java)

        withContext(Dispatchers.Main) {
            try {
                val response = service?.getAllPosts()
                if (response?.isSuccessful == true) {
                    response.body()?.let {
                        postsDao.updatePosts(it)
                        DataValidationUtils.setDataRefreshTime(context)

                        responseResult.value = it
                    }
                } else {
                    Toast.makeText(context, "Error fetching posts", Toast.LENGTH_SHORT).show()
                }
            } catch (error: HttpException) {
                Toast.makeText(context, error.message(), Toast.LENGTH_SHORT).show()
                responseResult.value = null
            } catch (error: Throwable) {
                Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT).show()
                responseResult.value = null
            }
        }

        return responseResult
    }

    suspend fun fetchUserById(userId: Int): MutableLiveData<UserModel>? {
        val responseResult = MutableLiveData<UserModel>()

        val service = RetrofitClientInstance.getInstance()?.create(UsersApi::class.java)

        withContext(Dispatchers.Main) {
            try {
                val response = service?.getUserById(userId)
                if (response?.isSuccessful == true) {
                    responseResult.value = response.body()
                } else {
                    Toast.makeText(context, "Error fetching user", Toast.LENGTH_SHORT).show()
                }

            } catch (error: HttpException) {
                Toast.makeText(context, error.message(), Toast.LENGTH_SHORT).show()
                responseResult.value = null
            } catch (error: Throwable) {
                Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT).show()
                responseResult.value = null
            }
        }

        return responseResult
    }

    suspend fun deletePost(postId: Int) {
        postsDao.deletePost(postId)
    }
}