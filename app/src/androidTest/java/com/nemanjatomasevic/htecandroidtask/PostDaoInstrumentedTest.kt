package com.nemanjatomasevic.htecandroidtask

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nemanjatomasevic.htecandroidtask.database.PostsRoomDatabase
import com.nemanjatomasevic.htecandroidtask.model.PostModel
import com.nemanjatomasevic.htecandroidtask.model.PostsDAO
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class PostDaoInstrumentedTest {

    @get:Rule val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val context: Context = ApplicationProvider.getApplicationContext()

    private lateinit var postsDao: PostsDAO
    private lateinit var db: PostsRoomDatabase

    @Before
    fun createDb() {
        db = Room.inMemoryDatabaseBuilder(context, PostsRoomDatabase::class.java)
            .allowMainThreadQueries()
            .build()

        postsDao = db.postsDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun getAllPosts() {
        val post = PostModel(1, 1, null, null)
        val post2 = PostModel(2, 2, null, null)

        runBlocking {
            postsDao.insertPost(post)
            postsDao.insertPost(post2)
        }

        val posts: List<PostModel>? = LiveDataTestUtil.getValue(postsDao.getAllPosts())
        assertEquals(posts?.get(0), post)
        assertEquals(posts?.get(1), post2)
    }

    @Test
    fun deletePost() {
        val post = PostModel(1, 1, null, null)
        val post2 = PostModel(2, 2, null, null)

        runBlocking {
            postsDao.insertPost(post)
            postsDao.insertPost(post2)

            postsDao.deletePost(1)
        }

        val posts: List<PostModel>? = LiveDataTestUtil.getValue(postsDao.getAllPosts())
        assertEquals(posts?.size, 1)
        assertEquals(posts?.get(0)?.id, 2)
    }

    @Test
    fun updatePosts() {
        val post = PostModel(1, 1, null, null)
        val post2 = PostModel(2, 2, null, null)

        runBlocking {
            postsDao.insertPost(post)
            postsDao.insertPost(post2)
        }

        val post3 = PostModel(1, 1, null, "test body")
        val post4 = PostModel(2, 2, "test title", null)
        val post5 = PostModel(3, 3, "test title", "test body")
        val postsToUpdate = ArrayList<PostModel>()
        postsToUpdate.add(post3)
        postsToUpdate.add(post4)
        postsToUpdate.add(post5)

        runBlocking {
            postsDao.updatePosts(postsToUpdate)
        }

        val posts: List<PostModel>? = LiveDataTestUtil.getValue(postsDao.getAllPosts())
        assertEquals(posts?.size, 3)
        assertEquals(posts?.get(0)?.body, post3.body)
        assertEquals(posts?.get(1)?.title, post4.title)
        assertEquals(posts?.get(2)?.title, post5.title)
    }
}